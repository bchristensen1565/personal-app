package database;



import java.sql.*;
import java.util.*;

public class connection {
    public static void main(String[] args) throws Exception, InputMismatchException {
        getConnection();
        System.out.print("============================================================= \n");
        System.out.print("|  Welcome to Yellowstone Surgery Center Computer Database   |\n");
        System.out.print("============================================================= \n");
        System.out.print("Please select what you would like to do: \n");
        System.out.print(" Type 1 = Enter Data \n");
        System.out.print(" Type 2 = Show Database \n");
        System.out.print(" Type 3 : Exit Program \n");
        System.out.print(" Your Entry: ");

        int x = 1;
        do {
            try {
                Scanner input = new Scanner(System.in);
                int inputEntry = 0;
                inputEntry = input.nextInt();
                if (inputEntry == 1) {
                    System.out.print("Now inputting Data: \n");
                    insert();
                }

                if (inputEntry == 2) {
                    System.out.print("Now Showing Database: \n");
                    show();
                }

                if (inputEntry == 3) {
                    System.out.print("Thank you for using the YSC Database! Come again soon!");
                    System.exit(0);

                } else {
                    System.out.print("Input Not recognizable Please Try Again");
                }

            } catch (Exception i) {
                i.printStackTrace();
                System.out.print("Invalid Input Please Try Again: ");
            }

            x=2;

        } while (x == 2);
    }

    public static void prompt() throws Exception {
        System.out.print("Please select what you would like to do: \n");
        System.out.print(" Type 1 = Enter Data \n");
        System.out.print(" Type 2 = Show Database \n");
        System.out.print(" Type 3 : Exit Program \n");
        System.out.print(" Your Entry: ");
        Scanner promptInput = new Scanner(System.in);
        int inputEntry = 0;
        inputEntry = promptInput.nextInt();
        if (inputEntry == 1) {
            System.out.print("Now inputting Data: \n");
            insert();
        }

        if (inputEntry == 2) {
            System.out.print("Now Showing Database: \n");
            show();
        }

        if (inputEntry == 3) {
            System.out.print("Thank you for using the YSC Database! Come again soon!");
            getConnection().close();
            System.exit(0);
        }
    }

    public static Connection getConnection() throws Exception{
        try {

            String url = "jdbc:mysql://localhost:3306/final";
            String user = "root";
            String password = "cit360rootpassword";


            Connection conn = DriverManager.getConnection(url,user,password);
            return conn;
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Database is down. Please wait for maintanence");
        }

        return null;
    }

    public static void insert() throws Exception {
        Scanner input = new Scanner(System.in).useDelimiter("\n");

        Integer computerID = Types.NULL;
        int x = 1;
        do {
            try {
                System.out.print("Enter Computer Name: ");
                String computerName = input.next();
                System.out.println("Computer Name is : " + computerName);
                if(computerName == "") {
                    System.out.print("Computer Name Cannot be Empty: Please Try Again:");
                    insert();
                }

                System.out.print("Enter Computer Model: ");
                String computerModel = input.next();
                System.out.println("Computer Model is : " + computerModel);

                System.out.print("Enter Computer Processor: ");
                String computerProcessor = input.next();
                System.out.print("Computer Processor is : " + computerProcessor);



                System.out.printf("%n");

                Statement st = null;
                Connection connection = getConnection();
                st = connection.createStatement();

                //st.executeUpdate("INSERT INTO computers value( 'null' " +", '"+computerName+"', '"+computerModel+"', '"+computerProcessor+"')");
                st.executeUpdate("INSERT INTO computers value('" + computerID + "', '" + computerName + "', '" + computerModel + "', '" + computerProcessor + "')");
                x=2;
            } catch (Exception e) {
                System.out.println("Error, Invalid Input. Please try again");
            }
        } while (x == 1);

        System.out.printf("Upload complete. Thanks for Uploading! \n");
        Connection conn = getConnection();
        conn.close();
        System.out.print("\n");
        prompt();
    }

    public static ArrayList show() throws Exception {
        System.out.print("Showing Database: \n");

        try {
            Connection conn = getConnection();
            PreparedStatement statement = conn.prepareStatement("SELECT * FROM computers");

            ResultSet rs = statement.executeQuery();

            ArrayList<String> sqlData = new ArrayList<String>() {
                @Override
                public int size() {
                    return 0;
                }

                @Override
                public boolean isEmpty() {
                    return false;
                }

                @Override
                public boolean contains(Object o) {
                    return false;
                }

                @Override
                public Iterator<String> iterator() {
                    return null;
                }

                @Override
                public Object[] toArray() {
                    return new Object[0];
                }

                @Override
                public <T> T[] toArray(T[] a) {
                    return null;
                }

                @Override
                public boolean add(String s) {
                    return false;
                }

                @Override
                public boolean remove(Object o) {
                    return false;
                }

                @Override
                public boolean containsAll(Collection<?> c) {
                    return false;
                }

                @Override
                public boolean addAll(Collection<? extends String> c) {
                    return false;
                }

                @Override
                public boolean removeAll(Collection<?> c) {
                    return false;
                }

                @Override
                public boolean retainAll(Collection<?> c) {
                    return false;
                }

                @Override
                public void clear() {

                }
            };

            while (rs.next()) {
                int computerID = rs.getInt("computerID");
                String computerName = rs.getString("computerName");
                String computerModel = rs.getString("computerModel");
                String processor = rs.getString("processor");

                System.out.print("============================================================================================================= \n");
                System.out.print(" Computer ID: " + computerID + " | ");
                System.out.print(" Computer Name: " + computerName + " | ");
                System.out.print(" Computer Model : " + computerModel + " | ");
                System.out.print(" Computer Processor: " + processor + "  |\n");
                sqlData.add(rs.getString("computerName"));
            }
            System.out.println(" \n Showing all computers in Database");
            System.out.print("\n");
            prompt();
            return sqlData;

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

    }
}




